/*
 * Project Fixer using has base: OpenCV_ex_16.c by J: Madeira - Dec 2012 + Nov 2017
 *
 * Dinis Canastro (nº 80299) & João de Jesus (nº 80098)
 *
 * Compiling: g++ $(pkg-config --cflags --libs opencv) -std=c++11  main.cpp -o projq
 */


#include <iostream>
#include <stdio.h>
#include <math.h>


#include "opencv2/core/core.hpp"

#include "opencv2/imgproc/imgproc.hpp"

#include "opencv2/highgui/highgui.hpp"


using namespace cv;
using namespace std;


// AUXILIARY  FUNCTION

void printImageFeatures(const cv::Mat &image)
{
  std::cout << std::endl;

  std::cout << "Number of rows : " << image.size().height << std::endl;

  std::cout << "Number of columns : " << image.size().width << std::endl;

  std::cout << "Number of channels : " << image.channels() << std::endl;

  std::cout << "Number of bytes per pixel : " << image.elemSize() << std::endl;

  std::cout << std::endl;
}

// extracted from: https://stackoverflow.com/questions/4654636/how-to-determine-if-a-string-is-a-number-with-c
bool is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}



// MAIN

int main( int argc, char** argv )
{

  // Show the pattern guideline image

  Mat image;
  image = imread( "cv_testframe3.png", IMREAD_UNCHANGED );


  if (argc < 2) {
    // Tell the user how to run the program
    std::cerr << "Usage: " << argv[0] << " 'Webcam index (integer)'" << std::endl;
    return 1;
  }else if(is_number(argv[1])){
    std::cerr << "Usage: " << argv[0] << " 'Webcam index (integer)'" << std::endl;
    return 1;
  }

  if( image.empty())
  {
      // NOT SUCCESSFUL : the data attribute is empty

      std::cout << "Image file could not be open !!" << std::endl;

      return -1;
  }

  resize(image, image, Size(image.cols/1.25, image.rows/1.25));
  namedWindow("fixer", CV_WINDOW_NORMAL);
  // Doesn't work properly in OSX
  // cv::namedWindow("fixer", CV_WINDOW_NORMAL);//create new window
  // cv::setWindowProperty("fixer",CV_WND_PROP_FULLSCREEN,CV_WINDOW_FULLSCREEN);
  imshow("fixer", image);
  waitKey(0);
  destroyWindow("fixer");

  // TODO: UNCOMMENT THIS PART
  // Capturing a single picture
  cv::Mat frame;
  // Cam selection
  int option = std::stoi(argv[1]);
  cv::VideoCapture cap;
  if(!cap.open(option)) return 0;
  cap >> frame;

  // Displaying image result
  cv::namedWindow( "Test", cv::WINDOW_AUTOSIZE );
  imshow("Test", frame);

  // End clean up

  cv::waitKey(0);
  cv::destroyWindow( "Test" );

  // TODO: Change the temporary image to the picture


  // Segmentation of the image using Hough Circle detection (link: https://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/hough_circle/hough_circle.html)

  Mat gray;
  cvtColor( frame, gray, CV_BGR2GRAY );
  GaussianBlur( gray, gray, Size(9, 9), 2, 2 );
  //imshow("Oi",gray);

  // detect circles in the image
  vector<Vec3f> circles;
  int sensibility = 250; //Parameter to change the sensibility TODO: IF THE WRONG NUMBER OF CIRCLES IS FOUND RETRY WITH A DIFFERENT SENSIBILITY
  HoughCircles(gray,circles, CV_HOUGH_GRADIENT, 1, gray.rows/10, sensibility, 75, 0, 0);

  // Draw the circles detected TODO: COMMENT THIS
  // for( size_t i = 0; i < circles.size(); i++ )
  // {
  //   Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
  //   printf("%f , %f || %f\n", circles[i][0], circles[i][1], circles[i][2]);
  //   int radius = cvRound(circles[i][2]);
  //   // circle center
  //   circle( test_img, center, 3, Scalar(0,255,0), -1, 8, 0 );
  //   // circle outline
  //   circle( test_img, center, radius, Scalar(0,0,255), 3, 8, 0 );
  // }

  //Show your results
  //imshow( "Circles detection", test_img );


  // Distance calculation
  // Picture
  //Order
  Vec2f ordered [4];
  int min_index = 0;
  float min_dist = 10000;
  int max_index = 0;
  float max_dist = 0;
  // Search the minimum point and maximum point
  for( size_t i = 0; i < circles.size(); i++ ){
    int temp_dist = circles[i][0] + circles[i][1];
    if(temp_dist < min_dist){
      min_index = i;
      min_dist = temp_dist;
    }
    if(temp_dist > max_dist){
      max_index = i;
      max_dist = temp_dist;
    }
  }
  ordered[0][0] = circles[min_index][0];
  ordered[0][1] = circles[min_index][1];
  ordered[3][0] = circles[max_index][0];
  ordered[3][1] = circles[max_index][1];
  // Quadrants now defined using 2 points
  float center_x = (ordered[3][0] - ordered[0][0]) / 2 + ordered[0][0];
  float center_y = (ordered[3][1] - ordered[0][1]) / 2 + ordered[0][1];
  printf("Center: (%f,%f)\n", center_x, center_y);
  // Search the other 2 points
  for( size_t i = 0; i < circles.size(); i++ ){
    if((circles[i][0] > center_x) && (circles[i][1] < center_y)){
      ordered[1][0] = circles[i][0];
      ordered[1][1] = circles[i][1];
      break;
    }
  }
  for( size_t i = 0; i < circles.size(); i++ ){
    if((circles[i][0] < center_x) && (circles[i][1] > center_y)){
      ordered[2][0] = circles[i][0];
      ordered[2][1] = circles[i][1];
      break;
    }
  }

  //Control image
  Mat c_gray;
  cvtColor( image, c_gray, CV_BGR2GRAY );
  GaussianBlur( c_gray, c_gray, Size(9, 9), 2, 2 );
  //imshow("Control",c_gray);

  // detect circles in the image
  vector<Vec3f> c_circles;
  int c_sensibility = 200; //Parameter to change the sensibility TODO: IF THE WRONG NUMBER OF CIRCLES IS FOUND RETRY WITH A DIFFERENT SENSIBILITY
  HoughCircles(c_gray, c_circles, CV_HOUGH_GRADIENT, 1, c_gray.rows/8, c_sensibility, 100, 0, 0);

  Vec2f c_ordered [4];
  int c_min_index = 0;
  float c_min_dist = 10000;
  int c_max_index = 0;
  float c_max_dist = 0;
  // Search the minimum point and maximum point
  for( size_t i = 0; i < c_circles.size(); i++ ){
    int c_temp_dist = c_circles[i][0] + c_circles[i][1];
    if(c_temp_dist < c_min_dist){
      c_min_index = i;
      c_min_dist = c_temp_dist;
    }
    if(c_temp_dist > c_max_dist){
      c_max_index = i;
      c_max_dist = c_temp_dist;
    }
  }
  c_ordered[0][0] = c_circles[c_min_index][0];
  c_ordered[0][1] = c_circles[c_min_index][1];
  c_ordered[3][0] = c_circles[c_max_index][0];
  c_ordered[3][1] = c_circles[c_max_index][1];
  // Quadrants now defined using 2 points
  float c_center_x = (c_ordered[3][0] - c_ordered[0][0]) / 2 + c_ordered[0][0];
  float c_center_y = (c_ordered[3][1] - c_ordered[0][1]) / 2 + c_ordered[0][1];
  // Search the other 2 points
  for( size_t i = 0; i < c_circles.size(); i++ ){
    if((c_circles[i][0] > c_center_x) && (c_circles[i][1] < c_center_y)){
      c_ordered[1][0] = c_circles[i][0];
      c_ordered[1][1] = c_circles[i][1];
      break;
    }
  }
  for( size_t i = 0; i < c_circles.size(); i++ ){
    if((c_circles[i][0] < c_center_x) && (c_circles[i][1] > c_center_y)){
      c_ordered[2][0] = c_circles[i][0];
      c_ordered[2][1] = c_circles[i][1];
      break;
    }
  }

  //Comparison (data proccess)
  // Pushing both of them to the beguinning of the axis
  Vec2f temp;
  temp[0] = ordered[0][0];
  temp[1] = ordered[0][1];
  Vec2f c_temp;
  c_temp[0] = c_ordered[0][0];
  c_temp[1] = c_ordered[0][1];

  for( size_t i = 0; i < 4; i++ ){
   ordered[i][0] -= temp[0];
   ordered[i][1] -= temp[1];
   c_ordered[i][0] -= c_temp[0];
   c_ordered[i][1] -= c_temp[1];
  }
  // Resizing so they are kinda similar in size
  float ratio = sqrt((c_ordered[3][0] * c_ordered[3][1]) / (ordered[3][0] * ordered[3][1]));
  //printf("Ratio: %f\n", ratio);
  for( size_t i = 0; i < 4; i++ ){
    //printf("Old Point[%zu]: %f, %f \n", i, ordered[i][0], ordered[i][1]);
    ordered[i][0] *= ratio;
    ordered[i][1] *= ratio;
  }


  // Tests
  // for( size_t i = 0; i < 4; i++ )
  // {
  //   printf("Point[%zu]:%f, %f \n", i, ordered[i][0], ordered[i][1]);
  //   Point center(cvRound(ordered[i][0]), cvRound(ordered[i][1]));
  //   //printf("%f , %f || %f\n", circles[i][0], circles[i][1], circles[i][2]);
  //   //int radius = cvRound(circles[i][2]);
  //   // circle center
  //   circle( test_img, center, 3, Scalar(0,255,0), -1, 8, 0 );
  //   // circle outline
  //   circle( test_img, center, 50, Scalar(0,0,255), 3, 8, 0 );
  // }
  // for( size_t i = 0; i < 4; i++ )
  // {
  //   printf("Control Point[%zu]: %f, %f \n", i, c_ordered[i][0], c_ordered[i][1]);
  //   Point center(cvRound(c_ordered[i][0]), cvRound(c_ordered[i][1]));
  //   //printf("%f , %f || %f\n", circles[i][0], circles[i][1], circles[i][2]);
  //   //int radius = cvRound(circles[i][2]);
  //   // circle center
  //   circle( image, center, 3, Scalar(0,255,0), -1, 8, 0 );
  //   // circle outline
  //   circle( image, center, 50, Scalar(0,0,255), 3, 8, 0 );
  // }
  //
  // imshow("Teste 1",image);
  // imshow("Teste 2", test_img);
  // waitKey(0);
  // destroyAllWindows();

  // Perspective fix (Skew) (Base extracted from the example at http://opencvexamples.blogspot.com/2014/01/perspective-transform.html)
  // Input Quadilateral or Image plane coordinates
  Point2f inputQuad[4];
  // Output Quadilateral or World plane coordinates
  Point2f outputQuad[4];
  // Lambda Matrix
  Mat lambda( 2, 4, CV_32FC1 );
  // Set the lambda matrix the same type and size as input
  lambda = Mat::zeros( image.rows, image.cols, image.type() );

  // The 4 points that select quadilateral on the input , from top-left in clockwise order
  // These four pts are the sides of the rect box used as input
  inputQuad[0] = Point2f(ordered[0][0],ordered[0][1]);
  inputQuad[1] = Point2f(ordered[1][0],ordered[1][1]);
  inputQuad[2] = Point2f(ordered[2][0],ordered[2][1]);
  inputQuad[3] = Point2f(ordered[3][0],ordered[3][1]);
  // The 4 points where the mapping is to be done , from top-left in clockwise order
  outputQuad[0] = Point2f(c_ordered[0][0],c_ordered[0][1]);
  outputQuad[1] = Point2f(c_ordered[1][0],c_ordered[1][1]);
  outputQuad[2] = Point2f(c_ordered[2][0],c_ordered[2][1]);
  outputQuad[3] = Point2f(c_ordered[3][0],c_ordered[3][1]);

  // // Get the Perspective Transform Matrix i.e. lambda
  lambda = getPerspectiveTransform( inputQuad, outputQuad );
  Mat invert = lambda.inv();
  // Apply the Perspective Transform just found to the src image
  Mat output;
  warpPerspective(image,output,lambda,Size(image.cols,image.rows));


  // Show the end results (image inversely distorted)
  Mat oi;
  resize(output, oi, Size(output.cols/2, output.rows/2));
  // Finish
  namedWindow("Final Result", CV_WINDOW_AUTOSIZE);
  imshow( "Final Result", output );
  waitKey(0);
  destroyAllWindows();
  return 0;
}
